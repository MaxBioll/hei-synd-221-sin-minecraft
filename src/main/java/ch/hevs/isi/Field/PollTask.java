package ch.hevs.isi.Field;

import ch.hevs.isi.Field.BoolRegister;
import ch.hevs.isi.Field.FloatRegister;
import ch.hevs.isi.SmartControl.SmartControl;

import java.util.TimerTask;

public class PollTask extends TimerTask {

    /**
     * call the 2 poll methods (for FloatRegister and BoolRegister)
     */
    @Override
    public void run() {
        BoolRegister.poll();
        FloatRegister.poll();
        SmartControl.poll();
    }
}
