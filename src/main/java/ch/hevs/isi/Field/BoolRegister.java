package ch.hevs.isi.Field;

import ch.hevs.isi.Core.DataPointBool;

import java.util.HashMap;
import java.util.Map;


public class BoolRegister {
    protected DataPointBool dpb;
    protected int address;
    protected static Map<DataPointBool, BoolRegister> map = new HashMap<>();


    /**
     * Constructor of BoolRegister
     * Put the DataPointBool and this BoolRegister in the HashMap map
     * @param dpb : DataPointBool link to the BoolRegister
     * @param address : Address of the BoolRegister in the Minecraft map
     */
    public BoolRegister(DataPointBool dpb, int address){
        this.dpb = dpb;
        this.address = address;
        map.put(dpb, this);
    }

    /**
     *Call the writeBool function of the ModbusAccessor to write the values in the Minecraft map
     */
    public void write(){
        ModbusAccessor.getInstance().writeBool(address, dpb.getValue());
    }

    /**
     * Update the value of the DataPointBool with the value of the Minecraft map with the ModbusAccessor method readBool
     */
    public void read(){
        dpb.setValue(ModbusAccessor.getInstance().readBool(address));
    }

    /**
     * Get the BoolRegister link to the DataPointBool
     * @param dp : DataPointBool of which one we search the BoolRegister
     * @return the BoolRegister link to the DataPointBool
     */
    public static BoolRegister getRegisterFromDataPoint(DataPointBool dp){
        return map.get(dp);
    }

    /**
     * Method who update all the HashMap's DataPointBool with the values of the Minecraft map
     */
    public static void poll(){
        for(Map.Entry<DataPointBool,BoolRegister> entry : map.entrySet()){
            if(!(entry.getKey().isOutput())){
                entry.getValue().read();
            }
        }
    }
}
