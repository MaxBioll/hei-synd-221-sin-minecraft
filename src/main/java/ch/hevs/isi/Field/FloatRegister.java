package ch.hevs.isi.Field;

import ch.hevs.isi.Core.DataPointFloat;
import ch.hevs.isi.Field.ModbusAccessor;
import com.serotonin.modbus4j.sero.timer.SystemTimeSource;

import java.util.HashMap;
import java.util.Map;

public class FloatRegister {

    protected DataPointFloat dpf;
    protected int address;
    protected float range;
    protected float offset;
    protected static Map <DataPointFloat, FloatRegister> map = new HashMap<>();

    /**
     * Constructor of FloatRegister
     * Put the DataPointFloat and this FloatRegister in the HashMap map
     * @param dpf : DataPointFloat link to the FloatRegister
     * @param address : Address of the FloatRegister in the Minecraft map
     * @param range : range of the value
     * @param offset : offset of the value
     */
    public FloatRegister(DataPointFloat dpf, int address, float range, float offset){
        this.dpf = dpf;
        this.address = address;
        this.range = range;
        this.offset = offset;
        map.put(dpf, this);
    }

    /**
     *Call the writeFloat function of the ModbusAccessor to write the values in the Minecraft map
     */
    public void write(){
        ModbusAccessor.getInstance().writeFloat(address, (dpf.getValue()-offset)/range);
    }


    /**
     * Update the value of the DataPointFloat with the value of the Minecraft map with the ModbusAccessor method readFloat
     */
    public void read(){
        dpf.setValue(ModbusAccessor.getInstance().readFloat(address)*range+offset);
    }

    /**
     * Get the FloatRegister link to the DataPointFloat
     * @param dp : DataPointFloat of which one we search the FloatRegister
     * @return the FloatRegister link to the DataPointFloat
     */
    public static FloatRegister getRegisterFromDataPoint(DataPointFloat dp){
        return map.get(dp);
    }

    /**
     * Method who update all the HashMap's DataPointFloat with the values of the Minecraft map
     */
    public static void poll(){
        for(Map.Entry<DataPointFloat,FloatRegister> entry : map.entrySet()){
            if(!(entry.getKey().isOutput())){
                entry.getValue().read();
            }
        }
    }
}
