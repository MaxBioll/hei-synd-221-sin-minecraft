package ch.hevs.isi.Field;

import ch.hevs.isi.Core.DataPointBool;
import ch.hevs.isi.Core.DataPointFloat;
import ch.hevs.isi.utils.IConnector;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Timer;

/**
 * This class describes the FieldConnector component
 */
public class FieldConnector implements IConnector {

    private static FieldConnector fc = new FieldConnector();

    /**
     * Constructor of FieldConnector
     * Initialize all the registers of the Minecraft map
     */
    private FieldConnector(){
        new FloatRegister(new DataPointFloat("GRID_U_FLOAT", false), 89,1000, 0);
        new FloatRegister(new DataPointFloat("BATT_P_FLOAT", false), 57,6000, -3000);
        new FloatRegister(new DataPointFloat("BATT_CHRG_FLOAT", false), 49,1, 0);
        new FloatRegister(new DataPointFloat("SOLAR_P_FLOAT", false), 61,1500, 0);
        new FloatRegister(new DataPointFloat("WIND_P_FLOAT", false), 53,1000, 0);
        new FloatRegister(new DataPointFloat("COAL_P_FLOAT", false),81,600, 0);
        new FloatRegister(new DataPointFloat("COAL_AMOUNT", false),65,1,0 );
        new FloatRegister(new DataPointFloat("HOME_P_FLOAT", false), 101,1000,0 );
        new FloatRegister(new DataPointFloat("PUBLIC_P_FLOAT", false), 97,500,0 );
        new FloatRegister(new DataPointFloat("FACTORY_P_FLOAT", false), 105,2000,0 );
        new FloatRegister(new DataPointFloat("BUNKER_P_FLOAT", false), 93,500,0 );
        new FloatRegister( new DataPointFloat("WIND_FLOAT", false), 301,1,0 );
        new FloatRegister( new DataPointFloat("WEATHER_FLOAT", false), 305,1,0 );
        new FloatRegister(new DataPointFloat("WEATHER_FORECAST_FLOAT", false), 309,1,0 );
        new FloatRegister( new DataPointFloat("WEATHER_COUNTDOWN_FLOAT", false), 313,600,0 );
        new FloatRegister( new DataPointFloat("CLOCK_FLOAT", false), 317,1,0 );
        new FloatRegister(new DataPointFloat("SCORE", false),345,3600000,0);
        new FloatRegister(new DataPointFloat("COAL_SP", false),601,1,0);
        new FloatRegister(new DataPointFloat("FACTORY_SP", false),605,1,0);

        new FloatRegister(new DataPointFloat("REMOTE_COAL_SP", true),209,1,0);
        new FloatRegister(new DataPointFloat("REMOTE_FACTORY_SP", true),205,1,0);
        new FloatRegister( new DataPointFloat("FACTORY_ENERGY", false),341,3600000,0);


        new BoolRegister(new DataPointBool("SOLAR_CONNECT_SW", false),609);
        new BoolRegister(new DataPointBool("WIND_CONNECT_SW", false),613);

        new BoolRegister(new DataPointBool("REMOTE_SOLAR_SW", true),401);
        new BoolRegister(new DataPointBool("REMOTE_WIND_SW", true),405);

    }

    /**
     * Initialise new Field Register
     *
     * top row give name of colum, in this order:
     * Label;Unit;Input (Y/N);Output (Y/N);Address;Range;Offset;Type (Float = F, Bool = B)
     *
     * @param access : local access of a .csv DataPoint list
     */
    public void init(String access){

        /*
         Path orderPath = Paths.get(access);
        List<String> lines = null;
        try {
            lines = Files.readAllLines(orderPath);
        } catch (IOException e) {
            System.out.println("Impossible de lire le fichier");
        }
        if (lines.size() < 2) {
            System.out.println("Il n'y a pas de DataPoint dans le fichier");
            return;
        }

        for (int i = 1; i < lines.size(); i++) {
            String[] split = lines.get(i).split(";");
            if(split[7].compareTo("B")==0)
            {
                if(split[2].compareTo("Y")==0) {
                    DataPointBool dpb = new DataPointBool(split[0], true);
                    if(BoolRegister.getRegisterFromDataPoint(dpb) != null)
                        new BoolRegister(dpb, Integer.parseInt(split[4]));
                }else{
                    DataPointBool dpb = new DataPointBool(split[0], false);
                    if(BoolRegister.getRegisterFromDataPoint(dpb) != null)
                        new BoolRegister(dpb, Integer.parseInt(split[4]));
                }
            }
            else if(split[7].compareTo("F")==0){
                if(split[2].compareTo("Y")==0) {
                    DataPointFloat dpf = new DataPointFloat(split[0], true);
                    if(FloatRegister.getRegisterFromDataPoint(dpf) != null)
                        new FloatRegister(dpf,Integer.parseInt(split[4]),Integer.parseInt(split[5]),Integer.parseInt(split[6]));
                }else{
                    DataPointFloat dpf = new DataPointFloat(split[0], false);
                    if(FloatRegister.getRegisterFromDataPoint(dpf) != null)
                        new FloatRegister(dpf,Integer.parseInt(split[4]),Integer.parseInt(split[5]),Integer.parseInt(split[6]));
                }
            }
            else{
                System.out.println(split[7]);
            }
        }
        */

    }

    /**
     * return the singleton fc
     * @return singleton fc
     */
    public static FieldConnector getInstance(){
        return fc;
    }

    /**
     * Push the name and the value of the DataPointBool to the field
     * @param label : name of the DataPoint
     * @param value : value of the DataPoint
     */
    private void pushToField(String label, boolean value){
        System.out.println("Field; " + label + " = " + value);
    }

    /**
     * Push the name and the value of the DataPointFloat to the field
     * @param label : name of the DataPoint
     * @param value : value of the DataPoint
     */
    private void pushToField(String label, float value){
        System.out.println("Field; " + label + " = " + value);
    }

    /**
     * call the pushToField Function when a DataPoint has a new value
     * @param dpb : DataPoint who changed value
     */
    @Override
    public void onNewValue(DataPointBool dpb) {
        pushToField(dpb.getLabel(), dpb.getValue());
        BoolRegister br = BoolRegister.getRegisterFromDataPoint(dpb);
        br.write();
    }

    /**
     * call the pushToField Function when a DataPoint has a new value
     * @param dpf : DataPoint who changed value
     */
    @Override
    public void onNewValue(DataPointFloat dpf) {
        pushToField(dpf.getLabel(), dpf.getValue());
        FloatRegister fr = FloatRegister.getRegisterFromDataPoint(dpf);
        fr.write();
    }

    /**
     * Use a timer to call the method pollTask periodically
     */
    public static void startPollTask(){
        Timer pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(), 0, 5000);
    }
}
