package ch.hevs.isi.Field;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

import ch.hevs.isi.Database.DatabaseConnector;
import ch.hevs.isi.utils.Utility;

/**
 *
 */
public class ModbusAccessor extends Socket {

        private static ModbusAccessor ma = new ModbusAccessor("localhost", 1502);
        private Socket socket;
        private short tIndentifier = 999;

    /**
     * create a socket with the server name and the port
     * @param serverName : address of the minecraft map
     * @param portNumber : port for the socket
     */
    public ModbusAccessor(String serverName, int portNumber)
        {
            super();
            try{
                socket = new Socket(serverName, portNumber);
            }
            catch(IOException e1){
                e1.getMessage();
            }
        }

        public static ModbusAccessor getInstance(){
        return ma;
    }

    /**
     * Send a request to change a value in the Minecraft map
     * @param adress : address of the register we want to change
     */
        public void writeFloat(int adress, float value)
        {
            ByteBuffer ba = ByteBuffer.allocate(10);
            ba.put(0, (byte) 0x10);
            ba.putShort(1, (short) adress);
            ba.putShort(3, (short) 0x0002);
            ba.put(5, (byte) 0x04);
            ba.putFloat(6, value);


            ByteBuffer message = ByteBuffer.wrap(this.addMBAPHeader(ba.array()));
            try {
                socket.getOutputStream().write(message.array());
                //System.out.println(Utility.getHexString(message.array()));
                byte[] rec = new byte[12];
                socket.getInputStream().read(rec);
                //System.out.println(Utility.getHexString(rec));
            } catch (IOException e) {
                System.out.println("IO Exeption float write");
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println("other Exeption float write");
                e.printStackTrace();
            }
        }

    /**
     * Use the writeFloatMethod and convert to boolean value
     * @param address : address of the register we want to change
     */
        public void writeBool(int address, Boolean value)
        {
            if(value){
                writeFloat(address, 1f);
            }else{
                writeFloat(address, 0f);
            }
    }

    /**
     * Send a request on the socket and wait for the response
     * It request a value of the Minecraft map
     * @param address : address of the register we want to read
     * @return the value of the register
     */
        public float readFloat(int address)
        {
            ByteBuffer theBB = ByteBuffer.allocate(5);
            byte functionCode = 0x03;
            theBB.put(0, functionCode);
            theBB.putShort(1, (short) address);
            theBB.putShort(3, (short) 0x0002);
            byte[] outputStream = (addMBAPHeader(theBB).array());
            byte[] inputStream = new byte[13];

            try {

                socket.getOutputStream().write(outputStream);

                socket.getInputStream().read(inputStream);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            byte[] retval = new byte[4];
            retval[0] = inputStream[9];
            retval[1] = inputStream[10];
            retval[2] = inputStream[11];
            retval[3] = inputStream[12];

            return convByteToFloat(retval);
        }

    /**
     * Use the readFloatMethod and convert to boolean value
     * @param adress : address of the register we want to read
     * @return the value of the register
     */
    public boolean readBool(int adress)
        {
            float value = readFloat(adress);
            if(value == 0f){
                return false;
            }else{
                return true;
            }
        }

        private static byte[] convBoolToByte(boolean value)
        {
            ByteBuffer ba = ByteBuffer.allocate(2);
            if( value )
            {
                ba.putShort(0, (short) 0xff00);
            }else
            {
                ba.putShort(0, (short) 0x0000);
            }
            return ba.array();
        }

        private static boolean convByteToBool(byte[] array)
        {
            ByteBuffer ba = ByteBuffer.wrap(array);
            if( ba.getFloat(0) == (short)0xff00 )
            {
                return true;
            }else if( ba.getShort(0) == (short)0x0000 )
            {
                return false;
            }else
            {
                System.out.println("     ERROR; it's not a boolean");
                return false;
            }
        }

        private static byte[] convFloatToByte(float value)
        {
            ByteBuffer ba =  ByteBuffer.allocate(4);
            ba.putFloat(value);
            return ba.array();
        }

    /**
     * Convert a byte value to a float value
     * @param array : byte we want to convert
     * @return the float value
     */
    private static float convByteToFloat(byte[] array)
        {
            ByteBuffer ba = ByteBuffer.wrap(array);
            return ba.getFloat(0);
        }

    /**
     * Create the MBAP header with the datas of the message we want to send
     * @param datas : Datas of the request
     * @return the ByteBuffer composed with The MBAP Header and the datas
     */
        private byte[] addMBAPHeader(byte[] datas)
        {
            short length = (short) (datas.length + 1);
            byte[] retArray = new byte[length + 6];
            retArray[0] = (byte) ((tIndentifier >> 8) & 0xFF);
            retArray[1] = (byte) (tIndentifier & 0xFF);
            retArray[2] = 0;
            retArray[3] = 0;
            retArray[4] = (byte) ((length >> 8) & 0xFF);
            retArray[5] = (byte) (length & 0xFF);
            retArray[6] = 1; //RTU not used then = 1
            for (int i = 0; i < datas.length; i++) {
                retArray[7 + i] = datas[i];
            }
            return retArray;
        }

    /**
     * Create the MBAP header with the datas of the message we want to send
     * @param datas : Datas of the request
     * @return the ByteBuffer composed with The MBAP Header and the datas
     */
    private ByteBuffer addMBAPHeader(ByteBuffer datas) {
        short length = (short)(datas.array().length + 1);
        ByteBuffer retBB = ByteBuffer.allocate(length + 6);
        retBB.putShort(0, (short) 999);
        retBB.putShort(2, (short) 0x0000);
        retBB.putShort(4, length);
        retBB.put(6, (byte) 0x01);
        for(int i = 0; i < datas.array().length; i++){
            retBB.put(7+i, datas.array()[i]);
        }
        return retBB;
    }

        public static void main(String[] args) {
            ModbusAccessor client = new ModbusAccessor("172.22.22.59", 1502 );

            while (true)
            {
                if ( client.readBool( 401)) {
                    client.writeBool(401, false);
                } else {
                    client.writeBool(401, true);
                }

                if (!client.readBool( 405)) {
                    client.writeBool(405, true);
                } else {
                    client.writeBool(405, false);
                }


                float f = (float)Math.random();

                System.out.println("Coal");
                System.out.println(client.readFloat(601));
                System.out.println(client.readFloat(209));
                client.writeFloat(209, f );
                System.out.println(client.readFloat(209));

                System.out.println("Factory");
                System.out.println(client.readFloat(605));
                System.out.println(client.readFloat(205));
                client.writeFloat(205, f );
                System.out.println(client.readFloat(205));

                //Pause 20 secondes
                Utility.waitSomeTime(20000);
            }

        }
}


