package ch.hevs.isi.SmartControl;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.DataPointBool;
import ch.hevs.isi.Core.DataPointFloat;

import java.util.Timer;


/**
 * This class describes the SmartControl component
 */
public class SmartControl {

    /**
     * Constructor for SmartControl
     */
    public SmartControl(){

    }


    /**
     * changes the values to regulate the use of the battery
     */
    static void doControl() {

        float solarP = ((DataPointFloat)(DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT"))).getValue();

        int energieSave = 300;
        if(((DataPointFloat)(DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT"))).getValue() > 0.8) {
            ((DataPointBool) (DataPoint.getDataPointFromLabel("REMOTE_WIND_SW"))).setValue(false);
            ((DataPointBool) (DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW"))).setValue(false);
            if (((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() > 0.1f){
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() - 0.1f);
            }else if(((DataPointFloat)(DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() <= 0.1f){
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(0f);
            }
            if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() < 0.9f) {
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() + 0.1f);
            }else if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() >= 0.9f) {
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(1f);
            }
            System.out.println("0.8");
        }else if(((DataPointFloat)(DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT"))).getValue() < 0.3){
            ((DataPointBool)(DataPoint.getDataPointFromLabel("REMOTE_WIND_SW"))).setValue(true);
            ((DataPointBool)(DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW"))).setValue(true);
            if (((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() < 0.9f){
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() + 0.1f);
            }else if(((DataPointFloat)(DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() >= 0.9f){
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(1f);
            }
            if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() > 0.1f) {
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() - 0.1f);
            }else if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() <= 0.1f) {
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(0f);
            }
            System.out.println("0.3");
        }else {
            ((DataPointBool) (DataPoint.getDataPointFromLabel("REMOTE_WIND_SW"))).setValue(true);
            ((DataPointBool) (DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW"))).setValue(true);
            if((solarP-energieSave)/1000 > 1) {
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(1f);
            } else if((solarP-energieSave)/1000 > 0.5){
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue((solarP-energieSave) /1000);
                ((DataPointFloat)(DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(0f);
            } else  if(solarP == 0){
                if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() < 0.3f && ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() > 0.2f) {
                    ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(0.25f);
                }else if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() < 0.3f) {
                    ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() + 0.05f);
                }else if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() > 0.2f) {
                    ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).getValue() - 0.05f);
                }
                if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() < 0.6f && ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() > 0.4f) {
                    ((DataPointFloat)(DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(0.5f);
                }else
                if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() < 0.6f) {
                    ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() + 0.05f);
                }else if(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() > 0.4f) {
                    ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).getValue() - 0.05f);
                }
            } else {
                ((DataPointFloat) (DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"))).setValue((solarP-energieSave)/1000);
                ((DataPointFloat)(DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"))).setValue(0.5f-(solarP-energieSave)/1000);
            }
            System.out.println("0.3-0.8");
        }
    }


    public static void poll(){
        doControl();
    }

}
