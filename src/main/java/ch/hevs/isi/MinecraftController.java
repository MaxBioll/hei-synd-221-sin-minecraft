package ch.hevs.isi;

import ch.hevs.isi.Database.DatabaseConnector;
import ch.hevs.isi.Field.FieldConnector;
import ch.hevs.isi.Field.ModbusAccessor;
import ch.hevs.isi.Web.WebConnector;
import ch.hevs.isi.utils.Utility;

public class MinecraftController {
    public static boolean USE_MODBUS4J = false;


    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) {

        String dbHostName    = "influx.sdi.hevs.ch";
        String dbName        = "SIn05";
        String dbUserName    = "SIn05";
        String dbPassword    = "030ef0d92a7a9b9a3a567d4d19042b5d";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }
        System.out.println("Hello Workd");
        //Etrange "SIn05" print SInxx in a String
        DatabaseConnector.getInstance().init("SIn05", dbPassword, dbHostName, "SIn05");

        /**
        DataPointFloat a = new DataPointFloat("Test A", false);
        DataPointBool b = new DataPointBool("Test B", true);
        //*/
        FieldConnector.getInstance().init("C:/Users/Mariéthoz Cédric/Desktop/HES2/Sinf/uC/ElectricalAge/label.csv");
        WebConnector.getInstance().init();
        System.out.println("operationel");

        //FieldConnector.startPollTask();
    }
}
