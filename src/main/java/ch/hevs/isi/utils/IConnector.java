package ch.hevs.isi.utils;

import ch.hevs.isi.Core.DataPointBool;
import ch.hevs.isi.Core.DataPointFloat;

public interface IConnector {
    public void onNewValue(DataPointBool dpb);
    public void onNewValue(DataPointFloat dpf);
}
