package ch.hevs.isi.Database;

import ch.hevs.isi.Core.DataPointBool;
import ch.hevs.isi.Core.DataPointFloat;
import ch.hevs.isi.utils.IConnector;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

/**
 * This class describes the DataBaseConnector component
 */
public class DatabaseConnector implements IConnector {

    private static DatabaseConnector dbc = new DatabaseConnector();
    private String user = "", passeword = "", access = "", base = "";

    /**
     * Constructor of DatabaseConnector
     * Empty only for dbc
     */
    private DatabaseConnector(){
        System.out.println("And INIT  ?");
    }

    public void init(String user, String passeword, String access, String base){
        this.user = user;
        this.passeword = passeword;
        this.access = access;
        this.base = base;
    }

    /**
     * return the singleton dbc
     * @return singleton dbc
     */
    public static DatabaseConnector getInstance(){
        return dbc;
    }

    /**
     * Push the name and the value of the DataPointBool in the database
     * @param label : name of the DataPoint
     * @param value : value of the DataPoint
     */
    private void pushToDB(String label, boolean value){
        /**
         * Relate back to pushToDB whit a float value
         * true = 1
         * false = 0
         */

        if(value){
            pushToDB(label, 1f);
        }else{
            pushToDB(label, 0f);
        }
    }

    /**
     * Push the name and the value of the DataPointFloat in the database
     * @param label : name of the DataPoint
     * @param value : value of the DataPoint
     */
    private void pushToDB(String label, float value){
        System.out.println("DB; " + label + " = " + value);

        URL url;
        HttpURLConnection connection = null;

        try {
            url = new URL("https://influx.hev.ch/write?db=SIn05");
            
            connection = (HttpURLConnection) url.openConnection();

            String userpass = user + ":" + passeword;
            String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
            connection.setRequestProperty ("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Content-Type", "binary/octet-stream");
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);



            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

           

            //send POST request
            writer.write(label + " value=" + value);
            writer.flush();

            //for read answer
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            int responseCode = connection.getResponseCode();

            //check answer code
            if(responseCode == 204){
                System.out.println("It's right no message");
            }else if(200 < responseCode && responseCode < 300){
                System.out.println("It's right with message");
            }else{
                System.out.println("--ERROR--");
            }

            //read answer
            while((in.readLine())!=null){
                String s = in.readLine();
                System.out.println(s);
                in.skip(s.length());
            }

            writer.close();
            in.close();
            connection.disconnect();

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * call the pushToDB Function when a DataPoint has a new value
     * @param dpb : DataPoint who changed value
     */
    @Override
    public void onNewValue(DataPointBool dpb) {
        pushToDB(dpb.getLabel(), dpb.getValue());
    }

    /**
     * call the pushToDB Function when a DataPoint has a new value
     * @param dpf : DataPoint who changed value
     */
    @Override
    public void onNewValue(DataPointFloat dpf) {
        pushToDB(dpf.getLabel(), dpf.getValue());
    }
}


