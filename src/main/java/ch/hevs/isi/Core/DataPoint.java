package ch.hevs.isi.Core;


import ch.hevs.isi.Field.FieldConnector;
import ch.hevs.isi.SmartControl.SmartControl;

import java.util.HashMap;
import java.util.Map;

public abstract class DataPoint {
    protected boolean output;                                               //Output:true     Input:false
    private String label;                                                   //Label of DataPoint
    protected static Map<String, DataPoint> dataPointMap = new HashMap<>();  //List of DataPoint

    /**
     * Constructor of the DataPoint
     * @param label : label of the DataPoint
     * @param output : if true the DataPoint is an output
     */
    public DataPoint(String label, boolean output)
    {
        this.output = output;
        this.label = label;
        dataPointMap.put(label,this);
    }

    /**
     * @return the label of the DataPoint
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * get the DataPoint from a label
     * @param s : Label
     * @return the DataPoint with label s
     */
    public static DataPoint getDataPointFromLabel(String s)
    {
        //Test if a DataPoint have label s
        if(dataPointMap.containsKey(s))
        {
            return dataPointMap.get(s);
        }
        else{
            System.out.println("null data point whit: " + s);
            return null ;
        }
    }

    /**
     * Return if the DataPoint is an output or an input
     * @return the boolean value output
     */
    public boolean isOutput(){
        return output;
    }


    public static void main(String[] args)
    {
        FieldConnector.startPollTask();
        /*
         * Test *
         dataPointMap.put("REMOTE_COAL_SP", new DataPointFloat("REMOTE_COAL_SP", true));            //REMOTE_COAL_SP    W     Y     Y      209     1        0
         dataPointMap.put("REMOTE_FACTORY_SP", new DataPointFloat("REMOTE_FACTORY_SP", true));      //REMOTE_FACTORY_SP %     Y     Y      205     1        0
         new FloatRegister(getDataPointFloatFromLabel("REMOTE_COAL_SP"), 209, 1, 0);
         new FloatRegister(getDataPointFloatFromLabel("REMOTE_FACTORY_SP"), 205, 1, 0);

         dataPointMap.put("REMOTE_SOLAR_SW", new DataPointBool("REMOTE_SOLAR_SW", true));           //REMOTE_SOLAR_SW   O/I   Y     Y      401     1        0
         dataPointMap.put("REMOTE_WIND_SW", new DataPointBool("REMOTE_WIND_SW", true));             //REMOTE_WIND_SW    O/I   Y     Y      405     1        0
         new BoolRegister(getDataPointBoolFromLabel("REMOTE_SOLAR_SW"), 401);
         new BoolRegister(getDataPointBoolFromLabel("REMOTE_WIND_SW"), 405);

         while (true)
         {
         if((getDataPointBoolFromLabel("REMOTE_SOLAR_SW")).getValue())
         {
         getDataPointBoolFromLabel("REMOTE_SOLAR_SW").setValue(false);
         getDataPointBoolFromLabel("REMOTE_WIND_SW").setValue(true);
         }else {
         getDataPointBoolFromLabel("REMOTE_SOLAR_SW").setValue(true);
         getDataPointBoolFromLabel("REMOTE_WIND_SW").setValue(false);
         }

         System.out.println("\n\n " + (getDataPointBoolFromLabel("REMOTE_SOLAR_SW")).getValue());

         float f = (float)Math.random();

         getDataPointFloatFromLabel("REMOTE_COAL_SP").setValue(f);
         getDataPointFloatFromLabel("REMOTE_FACTORY_SP").setValue(f);

         //Pause 20 secondes
         Utility.waitSomeTime(20000);
         }
         //*/



        /*
         * Test Field *
         DataPointFloat remoteFactoryDP = new DataPointFloat("REMOTE_FACTORY_SP", true);
         DataPointBool remoteWindDP = new DataPointBool("REMOTE_WIND_SW", true);
         FloatRegister remoteFactory = new FloatRegister(remoteFactoryDP, 205,1, 0);
         BoolRegister remoteWind = new BoolRegister(remoteWindDP, 405);
         remoteFactory.read();
         System.err.println("Remote Factory [%] :  " + remoteFactoryDP.getValue());
         remoteFactoryDP.setValue(0.2f);
         remoteFactory.write();
         System.err.println("Remote Factory [%] :  " + remoteFactoryDP.getValue());

         System.err.println("Remote Wind [ON/OFF] :  " + remoteWindDP.getValue());
         remoteWind.read();
         remoteWindDP.setValue(true);
         remoteWind.write();
         System.err.println("Remote Wind [ON/OFF] :  " + remoteWindDP.getValue());
         //*/



        /*
         * Test according to the video *
         DataPointFloat dp1 = new DataPointFloat("Test_input", false);
         dp1.setValue(33f);

         new DataPointBool("Test_output", true);

         DataPointBool bdp;
         bdp = getDataPointBoolFromLabel("Test_output");
         bdp.setValue(true);

         * Label *
         //Label             Unit  Input Output Address Range    Offset
         dataPointMap.put("GRID_U_FLOAT", new DataPointFloat("GRID_U_FLOAT", false));                //GRID_U_FLOAT      V     Y     N      89      1000     0
         dataPointMap.put("BATT_P_FLOAT", new DataPointFloat("BATT_P_FLOAT", false));                //BATT_P_FLOAT      W     Y     N      57      6000     -3000
         dataPointMap.put("BATT_CHRG_FLOAT", new DataPointFloat("BATT_CHRG_FLOAT", false));          //BATT_CHRG_FLOAT   %     Y     N      49      1        0
         dataPointMap.put("SOLAR_P_FLOAT", new DataPointFloat("SOLAR_P_FLOAT", false));              //SOLAR_P_FLOAT     W     Y     N      61      1500     0
         dataPointMap.put("WIND_P_FLOAT", new DataPointFloat("WIND_P_FLOAT", false));                //WIND_P_FLOAT      W     Y     N      53      1000     0
         dataPointMap.put("COAL_P_FLOAT", new DataPointFloat("COAL_P_FLOAT", false));                //COAL_P_FLOAT      W     Y     N      81      600      0
         dataPointMap.put("COAL_AMOUNT", new DataPointFloat("COAL_AMOUNT", false));                  //COAL_AMOUNT       %     Y     N      65      1        0
         dataPointMap.put("HOME_P_FLOAT", new DataPointFloat("HOME_P_FLOAT", false));                //HOME_P_FLOAT      P     Y     N      101     1000     0
         dataPointMap.put("PUBLIC_P_FLOAT", new DataPointFloat("PUBLIC_P_FLOAT", false));            //PUBLIC_P_FLOAT    P     Y     N      97      500      0
         dataPointMap.put("FACTORY_P_FLOAT", new DataPointFloat("FACTORY_P_FLOAT", false));          //FACTORY_P_FLOAT   P     Y     N      10      2000     0
         dataPointMap.put("BUNKER_P_FLOAT", new DataPointFloat("BUNKER_P_FLOAT", false));            //BUNKER_P_FLOAT    P     Y     N      93      500      0
         dataPointMap.put("WIND_FLOAT", new DataPointFloat("WIND_FLOAT", false));                    //WIND_FLOAT        %     Y     N      301     1        0
         dataPointMap.put("WEATHER_FLOAT", new DataPointFloat("WEATHER_FLOAT", false));              //WEATHER_FLOAT     %     Y     N      305     1        0
         dataPointMap.put("WEATHER_FORECAST_FLOAT", new DataPointFloat("WEATHER_FORECAST_FLOAT", false));    //WEATHER_FORECAST_FLOAT      %     Y     N      309     1       0
         dataPointMap.put("WEATHER_COUNTDOWN_FLOAT", new DataPointFloat("WEATHER_COUNTDOWN_FLOAT", false));  //WEATHER_COUNTDOWN_FLOAT     s     Y     N      313     600     0
         dataPointMap.put("CLOCK_FLOAT", new DataPointFloat("CLOCK_FLOAT", false));                  //CLOCK_FLOAT       %     Y     N      317     1        0

         dataPointMap.put("REMOTE_COAL_SP", new DataPointFloat("REMOTE_COAL_SP", true));            //REMOTE_COAL_SP    W     Y     Y      209     1        0
         dataPointMap.put("REMOTE_FACTORY_SP", new DataPointFloat("REMOTE_FACTORY_SP", true));      //REMOTE_FACTORY_SP %     Y     Y      205     1        0
         dataPointMap.put("REMOTE_SOLAR_SW", new DataPointBool("REMOTE_SOLAR_SW", true));           //REMOTE_SOLAR_SW   O/I   Y     Y      401     1        0
         dataPointMap.put("REMOTE_WIND_SW", new DataPointBool("REMOTE_WIND_SW", true));             //REMOTE_WIND_SW    O/I   Y     Y      405     1        0
         dataPointMap.put("FACTORY_ENERGY", new DataPointFloat("FACTORY_ENERGY", true));            //FACTORY_ENERGY    J     Y     Y      341     3600000  0

         dataPointMap.put("SCORE", new DataPointFloat("SCORE", false));                              //SCORE             -     Y     N      345     3600000  0
         dataPointMap.put("COAL_SP", new DataPointFloat("COAL_SP", false));                          //COAL_SP           %     Y     N      601     1        0
         dataPointMap.put("FACTORY_SP", new DataPointFloat("FACTORY_SP", false));                    //FACTORY_SP        %     Y     N      605     1        0
         dataPointMap.put("SOLAR_CONNECT_SW", new DataPointBool("SOLAR_CONNECT_SW", false));         //SOLAR_CONNECT_SW  O/I   Y     N      609     1        0
         dataPointMap.put("WIND_CONNECT_SW", new DataPointBool("WIND_CONNECT_SW", false));           //WIND_CONNECT_SW   O/I   Y     N      613     1        0

         //*/

    }

    /**
     * Set the value of the DataPoint
     * @param value : float value for the DataPoint
     */
    public abstract void setValue(float value);
    /**
     * Set the value of the DataPoint
     * @param value : boolean value for the DataPoint
     */
    public abstract void setValue(boolean value);
}
