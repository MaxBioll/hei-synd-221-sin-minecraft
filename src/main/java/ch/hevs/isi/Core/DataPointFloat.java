package ch.hevs.isi.Core;

import ch.hevs.isi.Database.DatabaseConnector;
import ch.hevs.isi.Field.FieldConnector;
import ch.hevs.isi.Web.WebConnector;

public class DataPointFloat extends DataPoint {
    private float value = 0;

    /**
     * Constructor of DataPointFloat
     * Call the constructor of the DataPoint
     * @param Label : label of the DataPoint
     * @param Output : if true the DataPoint is an output
     */
    public DataPointFloat(String Label, boolean Output) {
        super(Label, Output);
    }

    /**
     * Set the value of the DataPointFloat with a float parameter
     * @param value : float value for the DataPointFloat
     */
    @Override
    public void setValue(float value)
    {
        this.value = value;
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);
        //If DataPoint is a output
        if(output)
            FieldConnector.getInstance().onNewValue(this);
    }

    /**
     * Set the value of the DataPointFloat with a boolean parameter
     * Call the method set value with the float parameter
     * @param value : boolean value for the DataPointFloat
     */
    @Override
    public void setValue(boolean value) {
        if(value){
            this.setValue(1f);
        }else{
            this.setValue(0f);
        }
    }

    /**
     *
     * @return the value of the DataPointFloat
     */
    public float getValue() {
        return value;
    }

}
