package ch.hevs.isi.Core;

import ch.hevs.isi.Database.DatabaseConnector;
import ch.hevs.isi.Field.FieldConnector;
import ch.hevs.isi.Web.WebConnector;

public class DataPointBool extends DataPoint{
    boolean value;

    /**
     * Constructor of DataPointBool
     * Call the constructor of the DataPoint
     * @param Label : label of the DataPoint
     * @param Output : if true the DataPoint is an output
     */
    public DataPointBool(String Label, boolean Output) {
        super(Label, Output);
    }

    /**
     * Set the value of the DataPointBool with a float parameter
     * Call the method set value with the boolean parameter
     * @param value : float value for the DataPointBool
     */
    @Override
    public void setValue(float value) {
        if(value == 1f){
            this.setValue(true);
        }else{
            this.setValue(false);
        }
    }

    /**
     * set the value of the DataPointBool with a boolean parameter
     * @param a : boolean value for the DataPointBool
     */
    @Override
    public void setValue(boolean a){
        value = a;
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);
        //If DataPoint is a output
        if(output)
            FieldConnector.getInstance().onNewValue(this);
    }

    /**
     *
     * @return the value of the DataPointBool
     */
    public boolean getValue(){
        return value;
    }

}
