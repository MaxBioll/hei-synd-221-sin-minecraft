package ch.hevs.isi.Web;


import ch.hevs.isi.Core.DataPointBool;
import ch.hevs.isi.Core.DataPointFloat;
import ch.hevs.isi.utils.IConnector;

import java.net.InetSocketAddress;



/**
 * This class describes the WebConnector component
 */
public class WebConnector implements IConnector {

    private static WebConnector wc = new WebConnector();
    private static Server wss = new Server(new InetSocketAddress( 8888));

    /**
     * Constructor of WebConnector
     * Empty
     */
    private WebConnector(){
        //Empty
    }

    public void init()
    {
        wss.broadcast("WELCOM INIT");
    }

    /**
     * return the singleton wc
     * @return singleton wc
     */
    public static WebConnector getInstance(){
        return wc;
    }

    /**
     * Push the name and the value of the DataPointBool to the web pages
     * @param label : name of the DataPoint
     * @param value : value of the DataPoint
     */
    private void pushToWebPages(String label, boolean value){

        if(value){
            pushToWebPages(label, 1f);
        }else{
            pushToWebPages(label, 0f);
        }
    }

    /**
     * Push the name and the value of the DataPointFloat to the web pages
     * @param label : name of the DataPoint
     * @param value : value of the DataPoint
     */
    private void pushToWebPages(String label, float value){
        System.out.println("Web; " + label + " = " + value + "\n\n");
        wss.broadcast(label + "=" + value);
    }

    /**
     * call the pushToWebPages Function when a DataPoint has a new value
     * @param dpb : DataPoint who changed value
     */
    @Override
    public void onNewValue(DataPointBool dpb) {
        pushToWebPages(dpb.getLabel(), dpb.getValue());
    }

    /**
     * call the pushToWebPages Function when a DataPoint has a new value
     * @param dpf : DataPoint who changed value
     */
    @Override
    public void onNewValue(DataPointFloat dpf) {
        pushToWebPages(dpf.getLabel(), dpf.getValue());
    }
}
