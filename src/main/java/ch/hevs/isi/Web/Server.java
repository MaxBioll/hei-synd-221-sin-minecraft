package ch.hevs.isi.Web;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;

import static ch.hevs.isi.Core.DataPoint.*;


public class Server extends WebSocketServer {


    public Server(InetSocketAddress address)
    {
            super(address);
            this.start();
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        conn.send("Welcome to the server!"); //This method sends a message to the new client
        broadcast( "new connection: " + handshake.getResourceDescriptor() ); //This method sends a message to all clients connected
        System.out.println("new connection to " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        System.out.println("received message from "	+ conn.getRemoteSocketAddress() + ": " + message);

        String label = "";
        float value = 1;
        boolean l = true;
        for(int i = 0; i < message.length(); i++)
        {
            if(message.charAt(i) == '=') {
                l = false;
                i++;
            }
            if (l){
                label += message.charAt(i);
            }
            else if((message.charAt(i) == 't' || message.charAt(i) == 'f') && !l){
                if(message.charAt(i) == 't'){
                    value = 1f;
                }else{
                    value = 0f;
                }
                i = message.length();
            }
            else{
                message = message.substring(i);
                value = Float.parseFloat(message);
                i = message.length();
            }
        }

        getDataPointFromLabel(label).setValue(value);

    }


    @Override
    public void onError(WebSocket conn, Exception ex) {
        System.err.println("an error occurred on connection " + conn.getRemoteSocketAddress()  + ":" + ex);
    }

    @Override
    public void onStart() {
        System.out.println("server started successfully");
    }

}
